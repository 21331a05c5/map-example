import java.util.HashMap;
import java.util.Map;
public class MapExample {
    public static void main(String[] args) {
        Map<String, Integer> age = new HashMap<>();

        age.put("Jay",20);
        age.put("Jayden", 28);
        age.put("Sam",23);
    

        //printing out a value of a particular item
        int jayAge = age.get("Jay");
        System.out.println("Jay's age : " + jayAge);

        //checking if Chan is an object ffrom here or not
        boolean Chan = age.containsKey("Chan");
        System.out.println("Is Chan on the map?? " + Chan);

        //adding an item. Vaas
        age.put("Vaas",22);

        //removing an item. Jayden
        age.remove("Jayden");

     
        System.out.println("Ages : ");

        for (Map.Entry<String, Integer> entry : age.entrySet()) {
            String Agess = entry.getKey();
            int agesss = entry.getValue();
            System.out.println(Agess +" : " + agesss);
        }
    }
}

